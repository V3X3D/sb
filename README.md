# sb - Simple Blog

---

## What is sb?

`sb` is a shell script for blogging that expands onto [lb](https://github.com/LukeSmithxyz/lb) adding a few new and useful features.

The new features include
- Updating past posts - includes date, and title (lb can update now).
- Creating an index and keeping it up to date (all automatic).
- Setting a cutoff point on the rolling blog which links to the full post.
- Ability to set an amount of new posts you want to show on `index.html` page
- As well as some ease of use features.

## Usage?

- Place `sb` into the root of your project.
- Setup correct file structure (see example project that comes with `sb`).
- Use the `<!-- SB -->` flag to mark locations in the `index.html`, `blog.html`, and `rss.xml` (see example project and read the flags section).

### What are the commands?

```shell
# Make New Blog
./sb new     # Makes a new draft, which is stored in blog/.drafts, and edited with ./sb edit.
./sb publish # Publish a draft, this adds to your web pages using the `<!-- SB -->` flag.
# Modify Drafts
./sb edit    # Edit a draft of any type, meaning of type (p)ublished/(u)npublished.
./sb trash   # Discard a draft of any type, meaning of type (p)ublished/(u)npublished.
#Update a Blog
./sb change  # Change a published post, makes a draft which is stored in blog/.publishdrafts.
./sb update  # Update a published post, uses the draft made with the `./sb change`.
./sb delete  # Delete a published post, removes all references and deletes the file.

# Shortcuts Exist For All Commands
./sb n # Shortcut for `new`
./sb p # Shortcut for `publish`
./sb e # Shortcut for `edit`
./sb t # Shortcut for `trash`
./sb c # Shortcut for `change`
./sb u # Shortcut for `update`
./sb d # Shortcut for `delete`
```

## Flags?

Flags are just comments used by `sb` to find locations in a file.
For example the `<!-- SB -->` flag is used to place new entries and new entry links

### Flags explination

`<!-- SB -->`
In `blog.html` it will post the rolling blog you just published;
In `index.html` it will link to that blogs full entry.
Inside the blog-index file (this is auto created and is found in blog/index
folder) it will link to the blogs full entry.

`<!-- CUT -->`
This is used when creating a blog post to show that you want the rolling blog
preview to cut off and link to the standalone post.

`<!-- INDEX YEARS -->`
This is used in the previously mentioned blog-index file that gets auto created
and can be found in the blog/index folder. It tells sb to post links to previous
years blog-index files once a new year has come around.

`<!-- ENTRY END -->`
This is used to not the end of a rolling blog post, it helps for better visual
parsing if you need to go manually remove something because of an error. The
script also references this for the `./sb delete` and `./sb update` command.

## NOTES!

`sb` isn't posix.
It currently should work on all linux systems, running a modern bash shell.
RSS feeds should work though aren't tested at the moment.
